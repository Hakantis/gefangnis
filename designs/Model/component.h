#ifndef COMPONENT_H
#define COMPONENT_H

#include "ControllerIntf.h"

class Component:public ControllerIntf{
    public:
        Component(string name);
        ~Component();

        void start();
        void stop(bool exit);
};

#endif // COMPONENT_H
