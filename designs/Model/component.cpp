#include "Component.h"

Component::Component(string name):ControllerIntf(name){

}

Component::~Component(){}

void Component::start(){
    cout << "started: " << getName() << endl;
}

void Component::stop(bool exit){
    if(exit == true){
        cout << "stopped: " << getName() << endl;
    }
}
