/**
 * Project Untitled
 */


#include "ControllerIntf.h"


ControllerIntf::ControllerIntf(string name):name(name){

}

ControllerIntf::~ControllerIntf(){}

void ControllerIntf::start(){
    cout << "started: " << getName() << endl;
}

void ControllerIntf::stop(bool exit) {
    if(exit == true){
        cout << "stopped: " << getName() << endl;
    }
}

string ControllerIntf::getName() {
    return name;
}
