/**
 * Project Untitled
 */

#ifndef CONTROLLERINTF_H
#define CONTROLLERINTF_H

#include "hi.h"
#include <iostream>
using namespace std;

class ControllerIntf:public hi{
    public:
        ControllerIntf(string name);
        virtual ~ControllerIntf();

        virtual void start();
        virtual void stop(bool exit);
        string getName();

    private:
        string name;
};

#endif //CONTROLLERINTF_H
