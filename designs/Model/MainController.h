/**
 * Project Untitled
 */

#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include "ControllerIntf.h"
#include "../../gefangnis/designs/Model/Armbandverwaltung.h"
#include <iostream>
#include <vector>
using namespace std;

class MainController:public ControllerIntf {
    public:
        MainController(string name);
        ~MainController();

        void inject(ControllerIntf &controller);
        void start();
        void stop(bool exit = true);

    private:
        vector<ControllerIntf*> controllerIntf; //array von controllerinterface pointern
};

#endif //_MAINCONTROLLER_H
