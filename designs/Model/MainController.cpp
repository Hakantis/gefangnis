/**
 * Project Untitled
 */


#include "MainController.h"

/**
 * MainController implementation
 */

static bool started = false;
static bool stopped = false;


MainController::MainController(string name):ControllerIntf(name) {

}

MainController::~MainController(){}

void MainController::inject(ControllerIntf &controller) {
    if (!controllerIntf.empty()){ //wenn der zeiger auf controllerinterface nicht leer ist
        for(size_t i = 0; i < controllerIntf.size(); i++){ //gehe alle controllerinterfaceelemente durch
            if(controller.getName() == controllerIntf[i]->getName()){ //wenn es einen existierenden gibt
                ; //nichts tun
            }else{
                controllerIntf.push_back(&controller); //am ende von controllerIntf einfügen
            }
        }
    }else{ //wenn es keinen einzigen eintrag gibt
        controllerIntf.push_back(&controller); //in controllerIntf einfügen
    }
}

void MainController::start(){
    stopped = false; //stoppbar machen
    if(!started){ //wenn start funktion noch nie aufgerufen wurde
        ControllerIntf::start();
        for(size_t i = 0; i < controllerIntf.size(); i++){
            controllerIntf[i]->start();
        }
    }
    started = true; //es wurde einmal gestartet
}

void MainController::stop(bool exit){
    started = false; //startbar machen
    if(exit && !stopped){
        while(controllerIntf.size() != 0){ //solange ein controllerinterface existiert
            controllerIntf.back()->stop(true); //zugriff auf letztes element im vector und stop aufrufen
            controllerIntf.pop_back(); //letztes element löschen
        }
        ControllerIntf::stop(exit);
    }
    stopped = true; //es wurde einmal gestoppt
}
