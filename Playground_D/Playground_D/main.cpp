#include <QCoreApplication>
#include "../../gefangnis/designs/Model/MainController.h"
#include "../../gefangnis/designs/Model/Armbandverwaltung.h"
#include "../../gefangnis/designs/Model/component.h"

int main(int argc, char *argv[]){
    QCoreApplication a(argc, argv);

    //MainController *main = new MainController("MainController");
    MainController &main = *new MainController("Maincontroller");
    Component &armbandVerwaltung = *new Component("Armbandverwaltung");
    Component &personenVerwaltung = *new Component("Personenverwaltung");

    //main.inject(armbandVerwaltung);
    main.inject(personenVerwaltung);
    main.inject(personenVerwaltung);
    main.inject(personenVerwaltung);

    main.start();
    main.start();

    main.stop();
    main.stop();

    return a.exec();
}
