#ifndef CONTROLLERINTERFACE_H
#define CONTROLLERINTERFACE_H

#include <iostream>
using namespace std;
class ControllerInterface{
    public:
        ControllerInterface(string name){
            name = this->name;
        }
        virtual ~ControllerInterface();
        virtual void start() = 0;
        virtual void stop(bool exit) = 0;
        virtual string getName(){
            return name;
        }
    private:
        string name;
};

#endif // CONTROLLERINTERFACE_H
